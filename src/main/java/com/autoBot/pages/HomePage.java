package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class HomePage extends Annotations {
	
	public HomePage verifyLoginName(String data) {
		String loginName = driver.findElementByTagName("h2").getText();
		if(loginName.contains(data)) {
			System.out.println("Login success");
		}else {
			System.out.println("Logged username mismatch");
		}
		return this;
	}
	
	public MyHomePage clickCRMSFALink() {
		WebElement crmsfaLink = locateElement("link", "CRM/SFA");
		click(crmsfaLink);
		return new MyHomePage();
	}
	
	public LoginPage clickLogoutButton() {
		WebElement LogoutButton = locateElement("class", "decorativeSubmit");
		click(LogoutButton);
		return new LoginPage();
	}
	
	
	
	
	
	
	
	

}
