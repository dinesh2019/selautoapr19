package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class EditLeadPage extends Annotations {
	
	public EditLeadPage enterCompanyName(String updateComapanyName) {
		WebElement editCompanyName = driver.findElementById("updateLeadForm_companyName");
		editCompanyName.clear();
		editCompanyName.sendKeys(updateComapanyName);
		return this;
	}

	public ViewLeadPage clickUpdateButton() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}
}
