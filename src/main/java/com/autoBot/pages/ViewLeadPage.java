package com.autoBot.pages;

import com.autoBot.testng.api.base.Annotations;

public class ViewLeadPage extends Annotations{

	public void verifyFirstName(String fName) {
		String verifyFirstName = driver.findElementById("viewLead_firstName_sp").getText();

		if (verifyFirstName.equals(fName)) {
			System.out.println("User is created and first name matches");
		}
		else {
			System.out.println("User is created but first name is not matching");
		}

	}

	public EditLeadPage clickEditButton() {
		driver.findElementByXPath("//a[text()='Edit']").click();
		return new EditLeadPage();
	}

	public void verifyCompanyName(String cName) {
		String updatedCompanyName = driver.findElementById("viewLead_companyName_sp").getText();
		String[] compareCompanyName = updatedCompanyName.split(" ");
		System.out.println("Updated Company Name : "+compareCompanyName[0]);

		//Verify updated company name 

		if (compareCompanyName[0].equals(cName)) {
			System.out.println("Verification successful");	
		}
		else {
			System.out.println("Verification is not success");
		}

	}

	public DuplicateLeadPage clickDuplicateButton() {
		driver.findElementByXPath("//a[text()='Duplicate Lead']").click();
		return new DuplicateLeadPage();
	}

	public void verifyDuplicateLeadName(String leadFirstName) {
		String duplicateLeadName = driver.findElementById("viewLead_firstName_sp").getText();
		System.out.println("captured dup Name : " +duplicateLeadName);
		System.out.println("Given Lead Name : " +leadFirstName);
		if (duplicateLeadName.equals(leadFirstName)) {
			System.out.println("I Confirm that duplicated lead name is same as captured lead name");
		}
		else {
			System.out.println("Duplicate name does not match");
		}
	}
	
	public MyLeadsPage clickDeleteButton() {
		driver.findElementByXPath("//a[@class='subMenuButtonDangerous']").click();
		return new MyLeadsPage();
	}
}