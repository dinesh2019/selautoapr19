package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyLeadsPage extends Annotations{
	
	public CreateLeadPage clickCreateLeadMenu() {
		WebElement createLeadMenu = locateElement("link", "Create Lead");
		click(createLeadMenu);
		return new CreateLeadPage();
	}
	
	public FindLeadsPage clickFindLeadsMenu() {
		WebElement findLeadsMenu = locateElement("link", "Find Leads");
		click(findLeadsMenu);
		return new FindLeadsPage();
	}

}
