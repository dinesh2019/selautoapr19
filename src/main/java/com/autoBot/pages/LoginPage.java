package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class LoginPage extends Annotations {
	
	public LoginPage enterUserName(String data) {
		WebElement userName = locateElement("id", "username");
		clearAndType(userName, data);
		return this;
	}
	
	public LoginPage enterPassword(String data) {
		WebElement password = locateElement("id", "password");
		clearAndType(password, data);
		return this;
	}
	
	public HomePage clickLoginButton() {
		WebElement loginButton = locateElement("class", "decorativeSubmit");
		click(loginButton);
		/*HomePage hp = new HomePage();
		return hp;*/
		return new HomePage();
	}
	
	
	
	
	

}
