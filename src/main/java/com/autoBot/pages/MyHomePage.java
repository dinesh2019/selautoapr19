package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class MyHomePage extends Annotations {
	
	public MyLeadsPage clickLeadsMenu() {
		WebElement LeadsMenu = locateElement("link", "Leads");
		click(LeadsMenu);
		return new MyLeadsPage();
	}

}
