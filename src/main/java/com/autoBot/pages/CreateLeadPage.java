package com.autoBot.pages;

import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;

public class CreateLeadPage extends Annotations{
	
	public CreateLeadPage enterCompanyName(String cName) {
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(companyName, cName);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String fName) {
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(firstName, fName);
		return this;
	}
	
	public CreateLeadPage enterLastName(String lName) {
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(lastName, lName);
		return this;
		
	}
	
	public ViewLeadPage clickCreateLeadButton() {
		WebElement createLeadButton = locateElement("class", "smallSubmit");
		click(createLeadButton);
		return new ViewLeadPage();
	}

}
