package com.autoBot.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.autoBot.testng.api.base.Annotations;


public class FindLeadsPage extends Annotations {
	
	public FindLeadsPage enterFirstName(String fName) {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(fName);
		return this;
	}
	
	public FindLeadsPage enterLeadID(String leadID) {
		driver.findElementByXPath("//input[@name='id']").sendKeys(leadID);
		return this;
	}
	
	
	public FindLeadsPage clickEmailTab() {
		driver.findElementByXPath("//span[text()='Email']").click();
		return this;
	}
	public FindLeadsPage enterEmailID(String email) {
		driver.findElementByXPath("//input[@name='emailAddress']").sendKeys(email);
		return this;
	}
	public FindLeadsPage clickFindLeadsButton() throws InterruptedException {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		return this;
	}
	
	public FindLeadsPage captureFirstLeadID() {
		WebElement firstResultLeadID = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a");

		String capturedLeadID = firstResultLeadID.getText();
		System.out.println("Captured Lead ID : " + capturedLeadID);
		return this;
	}
	
	public ViewLeadPage clickFirstLeadID() {
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])[1]/a").click();
		return new ViewLeadPage();
	}
	
	public FindLeadsPage clickPhoneTab() {
		driver.findElementByXPath("//span[text()='Phone']").click();
		return this;
	}
	
	public FindLeadsPage enterCountryCode(String countryCode) {
		driver.findElementByXPath("//input[@name='phoneCountryCode']").sendKeys(Keys.BACK_SPACE, countryCode);
		return this;
	}
	
	public FindLeadsPage enterAreaCode(String areaCode) {
		driver.findElementByXPath("//input[@name='phoneAreaCode']").sendKeys(areaCode);
		return this;
	}
	
	public FindLeadsPage enterPhoneNumber(String phoneNumber) {
		driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys(phoneNumber);
		return this;
	}
	
	public void verifyDeletedLeadID(String delLeadID) {
		String errorMessage = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
		if (errorMessage.equals("No records to display"))
			System.out.println("The captured Lead ID: " + delLeadID + " is deleted");
	}

}


















