package com.autoBot.pages;

import com.autoBot.testng.api.base.Annotations;

public class DuplicateLeadPage extends Annotations {

	public void verifyPageTitle() {
		String title = driver.findElementById("sectionHeaderTitle_leads").getText();

		if(title.equals("Duplicate Lead")) {
			System.out.println("The title of the page is correct");
		}else {
			System.out.println("The title of the page is not correct");
		}
	}
	public DuplicateLeadPage leadFirstName() {
		String leadFirstName = driver.findElementById("createLeadForm_firstName").getAttribute("value");
		return this;
	}

	public ViewLeadPage clickCreateLeadButton() {
		driver.findElementByName("submitButton").click();
		return new ViewLeadPage();
	}

}
