package com.autoBot.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.autoBot.pages.LoginPage;
import com.autoBot.testng.api.base.Annotations;

public class TC002CreateLead extends Annotations {
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create a Lead";
		author = "Dinesh";
		category = "smoke";
		excelFileName = "TC002";
		
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password, String logInName, String companyName, String firstName, String lastName) {
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLoginButton()
		.verifyLoginName(logInName)
		.clickCRMSFALink()
		.clickLeadsMenu()
		.clickCreateLeadMenu()
		.enterCompanyName(companyName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickCreateLeadButton()
		.verifyFirstName(firstName);
		//.clickLogoutButton();
	}
	
	
	
	
	
	
	
	
	
	

}
